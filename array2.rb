def not_so_quick_sort(arr)
  return [] if arr.empty?

  pivot, *rest = arr
  smaller = rest.select { |x| x <= pivot }
  larger  = rest.select { |x| x > pivot }

  return not_so_quick_sort(smaller) + [pivot] + not_so_quick_sort(larger)
end

arr = Array.new(200) { rand(100) }
puts "arr: #{arr}"
puts ""
puts "sorted arr: #{not_so_quick_sort(arr)}"
