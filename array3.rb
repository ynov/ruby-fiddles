def rest(xs)
  return xs[1..-1]
end

def merge(xs, ys)
  if xs.empty?
    return ys
  elsif ys.empty?
    return xs
  end

  x = xs[0]; y = ys[0]
  if x <= y
    return [x] + merge(rest(xs), ys)
  else
    return [y] + merge(xs, rest(ys))
  end
end

def mergesort(arr)
  return arr if arr.length <= 1

  m = arr.length / 2
  arr_left  = arr[0..m-1]
  arr_right = arr[m..-1]

  return merge(mergesort(arr_left), mergesort(arr_right))
end

arr = Array.new(200) { rand(100) }
puts "arr: #{arr}"
puts ""
puts "sorted arr: #{mergesort(arr)}"
