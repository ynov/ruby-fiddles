def count_of_words(str)
  str.split(' ').inject(Hash.new(0)) do |counts, word|
    counts[word] += 1

    counts
  end
end

puts count_of_words('The quick brown fox jumps over the lazy dog')

# another version, using String#scan
def count_frequency(words)
  words.downcase.scan(/[\w']+/).inject(Hash.new(0)) do |counts, word|
    counts[word] += 1

    counts
  end
end

# old-fashioned version
def count_frequency_2(words)
  counts = Hash.new(0)

  word_list = words.downcase.scan(/[\w']+/)
  for word in word_list
    counts[word] += 1
  end

  counts
end

puts count_frequency("But I didn't inhale, he said (emphatically)")
puts count_frequency_2("But I didn't inhale, he said (emphatically)")

puts
# Hash#each and #Hash.sort_by
my_hash = { a: 100, b: 45, c: 125, d: 15, e: 25, f: 5 }
puts my_hash
my_hash.each do |(key, val)|
  puts "#{key} is #{val}"
end
puts "\nSorted by value:"
my_hash_sorted = my_hash.sort_by { |(key, val)| val }.to_h
# or:
# my_hash_sorted = my_hash.sort_by { |key, val| val }.to_h
my_hash_sorted.each do |(key, val)|
  puts "#{key} is #{val}"
end
