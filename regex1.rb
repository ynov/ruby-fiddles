# from RRP page 122
def show_regex(string, pattern)
  match = pattern.match(string)
  if match
    "#{match.pre_match}->#{match[0]}<-#{match.post_match}"
  else
    "no match for \"#{string}\""
  end
end

puts show_regex('hello, world!', /hello/)
puts show_regex('the emperor', /\Athe/)
puts show_regex('he was the emperor', /\Athe/)
s = 'red ball blue sky'
puts show_regex(s, /blue|red/)
puts show_regex(s, /(red|blue) \w+/)
puts show_regex(s, /red|blue \w+/)

#puts show_regex('hello', /(\w)\1/)
puts show_regex('hello', /(?<c>\w)\k<c>/)
#puts show_regex('mississippi', /(\w+)\1/)
puts show_regex('mississippi', /(?<s>\w+)\k<s>/)

/(?<hour>\d\d):(?<min>\d\d)(..)/ =~ "12:50am"
puts "Hour is #{hour}, minute #{min}"
