# since ruby 2.0
def f(foo: nil, bar: nil, qux: nil)
  s = ''

  s += "foo: #{foo}, " if !foo.nil?
  s += "bar: #{bar}, " if !bar.nil?
  s += "qux: #{qux}, " if !qux.nil?

  s
end

puts f(foo: 'foo', bar: 'bar', qux: 'qux')

# ruby 1.9 compatible
def g(opts = Hash.new)
  s = ''
  return s unless opts.is_a?(Hash)

  s += "foo: #{opts[:foo]}, " if !opts[:foo].nil?
  s += "bar: #{opts[:bar]}, " if !opts[:bar].nil?
  s += "qux: #{opts[:qux]}, " if !opts[:qux].nil?

  s
end

puts g(foo: 'foo', bar: 'bar', qux: 'qux')

puts f(foo: false, bar: 'hello, world!', qux: nil)
puts g(foo: nil, bar: false, qux: 'hi, world!')
