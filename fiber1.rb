words = Fiber.new do
  File.foreach('block1.rb') do |line|
    line.downcase.scan(/[\w]{3,}/) do |word|
      Fiber.yield word
    end
  end
end

counts = Hash.new(0)
while word = words.resume
  counts[word] += 1
end

counts.sort_by { |k,v | v }.reverse.to_h.each do |k, v|
  puts "#{k}: #{v}"
end
