def do_with_10
  yield(10)
end

x1 = do_with_10 { |p| p * 10 }
puts "x1: #{x1}"

half_it = lambda { |p| p / 2 }
x2 = do_with_10 &half_it
puts "x2: #{x2}"

def do_with_100(&block)
  block.call 10
end

x3 = do_with_100 do |x|
  x / 2
end
puts "x3 = #{x3}"

# from PPR 1.9 page 89
def my_while(cond, &block)
  while cond.call do
    block.call
  end
end

puts ""

a = 0
my_while lambda { a < 3} { puts a; a += 1}
#my_while ->{ a < 3 } { puts a; a += 1 }
