arr1 = [1, 4, 2, 5, 2, 1, 5, 2, 9]
puts "arr1: #{arr1}"

arr2 = arr1.map { |x| x + 1 }
puts "arr2: #{arr2}"

arr3 = arr1.select { |x| x < 5 }.sort
puts "arr3: #{arr3}"

arr4 = arr3.inject(0) { |acc, x| acc + x }
puts "arr4: #{arr4}"

def median(list)
  return nil if list.empty?

  sorted = list.sort
  m = sorted.length / 2

  if sorted.length.odd?
    return sorted[m]
  else
    return (sorted[m] + sorted[m - 1]) / 2.0
  end
end

list = [1, 2, 3, 4]
puts "median of #{list} is #{median(list)}"
list = [4, 3, 9]
puts "median of #{list} is #{median(list)}"
