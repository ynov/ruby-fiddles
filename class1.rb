class Vehicle
  def initialize(id, name)
    @id = id
    @name = name
  end

  def ==(other)
    @id == other.id && @name == other.name
  end

  def eql?(other)
    self == other
  end

  def hash
    @id.hash ^ @name.hash
  end

  def to_s
    "(#{@id}, #{@name})"
  end

  def inspect
    self.to_s
  end

  attr_accessor :id, :name
end

v1 = Vehicle.new 1, 'foo'
v2 = Vehicle.new 1, 'foo'
v3 = Vehicle.new 2, 'bar'
v4 = Vehicle.new 3, 'qux'
v5 = Vehicle.new 4, 'baz'

vs = [v1, v2, v3, v4, v5]

puts "v1: #{v1}"
puts "v2: #{v2}"
puts "v1 == v2: #{v1 == v2}"
puts "vs: #{vs}"
puts "vs.uniq: #{vs.uniq}"
