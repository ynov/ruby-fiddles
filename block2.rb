# from PPR 1.9 page 84
class File
  def self.open_and_process(*args)
    f = File.open(*args)
    yield f
    f.close()
  end
end

File.open_and_process('block1.rb', 'r') do |file|
  begin
    while line = file.readline do
      puts line
    end
  rescue EOFError
    # EOF
  end
end
