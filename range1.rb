def factorial(n)
  return 1 if n == 0
  (1..n).inject(1) { |acc, x| acc * x }
end

(0..12).each do |i|
  puts "factorial(#{i}): #{factorial(i)}"
end
puts

def fib_seq(n)
  (0..n - 3).inject([1, 1]) { |seq| seq << seq[-2] + seq[-1] }
end

def fib(n)
  a, b = 1, 1

  (n - 2).times do
    a, b = b, a + b
  end

  b
end

puts "fib_seq(16): #{fib_seq(16)}"
puts "fib(16): #{fib(16)}"
puts

even_numbers = (0..100).select { |i| i.even? }
puts "even_numbers: #{even_numbers}"
